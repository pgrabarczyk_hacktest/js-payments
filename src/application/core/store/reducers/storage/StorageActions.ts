export const SET_ITEM = 'STORAGE/SET_ITEM';

export interface ISetItemPayload {
  key: string;
  value: any;
}
