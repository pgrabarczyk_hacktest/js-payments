msgid ""
msgstr ""
"Project-Id-Version: i18next-conv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"POT-Creation-Date: 2019-06-04T22:42:51.818Z\n"
"PO-Revision-Date: 2019-06-04T22:42:51.818Z\n"
"Language: nl_NL\n"

msgid "Timeout"
msgstr "Timeout"

msgid "Field is required"
msgstr "Dit is een verplicht veld"

msgid "An error occurred"
msgstr "Er is iets misgegaan"

msgid "Merchant validation failure"
msgstr "Validatie verkoper is mislukt"

msgid "Payment has been cancelled"
msgstr "De betaling is geannuleerd"

msgid "Value mismatch pattern"
msgstr "Invoer voldoet niet aan patroon"

msgid "Invalid response"
msgstr "Ongeldig antwoord"

msgid "Invalid request"
msgstr "Ongeldig verzoek"

msgid "Value is too short"
msgstr "Invoer is te kort"

msgid "Payment has been authorized"
msgstr "De betaling is geautoriseerd"

msgid "Amount and currency are not set"
msgstr "Bedrag en valuta zijn niet ingesteld"

msgid "Payment has been successfully processed"
msgstr "De betaling is verwerkt"

msgid "Card number"
msgstr "Kaartnummer"

msgid "Expiration date"
msgstr "Vervaldatum"

msgid "Security code"
msgstr "Beveiligingscode"

msgid "Ok"
msgstr "De betaling is verwerkt"

msgid "Method not implemented"
msgstr "De methode is niet geïmplementeerd"

msgid "Form is not valid"
msgstr "Het formulier is ongeldig"

msgid "Pay"
msgstr "Betalen"

msgid "Processing"
msgstr "Bezig met verwerken"

msgid "Invalid field"
msgstr "Ongeldig veld"

msgid "Card number is invalid"
msgstr "Het kaartnummer is ongeldig"
