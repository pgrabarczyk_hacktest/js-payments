import { Selectors } from '../../../shared/Selectors';

export const NotificationsClasses = {
  error: Selectors.NOTIFICATION_FRAME_ERROR_CLASS,
  info: Selectors.NOTIFICATION_FRAME_INFO_CLASS,
  success: Selectors.NOTIFICATION_FRAME_SUCCESS_CLASS,
  cancel: Selectors.NOTIFICATION_FRAME_CANCEL_CLASS
};
