export interface IStorageState {
  [index: string]: any;
}

export const INITIAL_STATE: IStorageState = {};
